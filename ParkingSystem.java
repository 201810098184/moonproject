package learngit;
import java.util.ArrayList;
import java.util.Scanner;

public class ParkingSystem implements IParking{
    static Integer big;
    static Integer medium;
    static Integer small;

	//parse方法实现
	public String[] parse(String str) 
	{
		//String str=sc.nextLine();
		String str1=str.replace(" ", "");
		String str2=str1.replaceAll("\\\"", "");
		String str3=str2.replaceAll("\\[", "").replace("]", "");
		String[] i=str3.split(",");
		return i;
	}
    
	public ParkingSystem()
	{}
	
    public ParkingSystem(Integer big, Integer medium, Integer small)
    {
        this.big=big;
        this.medium=medium;
        this.small=small;
    }

    @Override
    public boolean addCar(int carType) {
        switch (carType)
        {
            case 1:
                if(this.big>0)
                {
                	this.big--;
                }
                else
                {
                    return false;
                }break;
            case 2 :
                if(this.medium>0)
                {
                	this.medium--;
                }
                else
                {
                    return false;
                }break;
            case 3:
                if(this.small>0)
                {
                	this.small--;
                }
                else
                {
                    return false;
                }break;
        }
        return true;
    }

    
    @Override
    public void print(String[] res) 
    {
    	System.out.print("[");
    	for(int i=0;i<res.length;i++)
    	{
    		if(i<res.length-1)
    		{
    			System.out.print(res[i]+",");
    		}
    		else
    		{
    			System.out.print(res[i]);
    		}
    	}
    	System.out.print("]");
    }
    public static void main(String[] args) throws Exception
    {
    	Scanner sc=new Scanner(System.in);
    	ParkingSystem ps=new ParkingSystem();
    	String s1=sc.nextLine();
    	String s2=sc.nextLine();
    	String[] arr=ps.parse(s1);
    	String[] arr1=ps.parse(s2);
    	int[] arr2=new int[arr1.length];
    	for(int i=0;i<arr1.length;i++)
    	{
    		arr2[i]=Integer.valueOf(arr1[i]);	
    	}
    	
    	IParams params=IParking.parse();
    	//IParams params=ParkingSystem.parse();
        
    	//车位数量
    	ParkingSystem ps2=new ParkingSystem(arr2[0],arr2[1],arr2[2]);

    	//车位大小
    	boolean[] arr3=new boolean[arr2.length-3];
    	for(int i=0;i<arr2.length-3;i++)
    	{
    		arr3[i]=ps.addCar(arr2[i+3]);
    	}

    	String[] result = new String[arr.length];
    	for(int i=0;i<arr.length;)
    	{
    		if(params==null)
	    	{
    			result[i]="null";
    			i++;
	    	}
    		for(int j=0;j<arr3.length;j++)
    		{
    			if(arr3[j]==true)
    			{
    				result[i]="true";
    				i++;
    			}
    			else if(arr3[j]==false)
    			{
    				result[i]="false";
    				i++;
    			}
    		}
	    }
        ps2.print(result);
        sc.close();
    }
}
